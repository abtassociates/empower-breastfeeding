<?php
/**
 * Enqueues child theme stylesheet, loading first the parent theme stylesheet.
 */
function themify_custom_enqueue_child_theme_styles() {
	wp_enqueue_style( 'parent-theme-css', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'themify_custom_enqueue_child_theme_styles', 11 );


/* function exclude_category($query) {
if ( $query->is_home() ) {
$query->set('cat', '-slider');
}
return $query;
}
add_filter('pre_get_posts', 'exclude_category'); */
?>
