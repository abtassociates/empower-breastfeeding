jQuery(document).ready(function() {
	jQuery('a#WordAppA').each(function() //Word Doc Application on Application Page
   		{this.setAttribute('onclick',"_gaq.push(['_trackEvent', 'WordAppA', 'Click', '" + this.href + "']);");});

	jQuery('a#WordAppOA').each(function() //Word Doc Application on Online Application Page
   		{this.setAttribute('onclick',"_gaq.push(['_trackEvent', 'WordAppOA', 'Click', '" + this.href + "']);");});

	jQuery('a.appbutton').each(function() //Application Link on Application Page and online application page
   		{this.setAttribute('onclick',"_gaq.push(['_trackEvent', 'ApplyButton'', 'Click', '" + this.href + "']);");});

	jQuery('a.webinar').each(function() //Webinar links
   		{this.setAttribute('onclick',"_gaq.push(['_trackEvent', 'Webinar'', 'Click', '" + this.href + "']);");});

	jQuery('a.slides').each(function() //Webinar slides
   		{this.setAttribute('onclick',"_gaq.push(['_trackEvent', 'Slides'', 'Click', '" + this.href + "']);");});

}